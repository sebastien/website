---
title: Resources
---
<br />

# <i class="fab fa-discourse"></i> Forum

If you run into problems with Dynare that you can’t seem to resolve by looking
at the manual, the [Dynare Forum](https://forum.dynare.org/) is a good place to
turn. If you’re a beginner, odds are someone else has run into the same problem
as you, so make sure you search the forum for similar questions. Otherwise,
feel free to make an account and ask a question yourself. If you’re a more
experienced user, heading to the forum from time to time will allow you to
learn the intricacies of Dynare and you might just be able to help out someone
else out!

# <i class="fas fa-envelope"></i> Mailing List

If you would like to be informed of recent Dynare news by email (new releases,
Summer School, and Conference dates), feel free to [sign up to receive
notifications](https://www.dynare.org/mailman3/postorius/lists/info.dynare.org/). This is a
low traffic list; you’ll receive at least 2 and on average 4 emails per year,
depending on how many releases we create.


# <i class="fas fa-graduation-cap"></i> Summer School

We organize a weeklong summer school every year in June where we teach students
how to use Dynare. The fee for students is minimal (equal to expenses on
lunches and one dinner) so as to encourage participation. If this interests
you, please keep a look out for the opening of the application period in April.


# <i class="fas fa-tachometer-alt"></i> Quick Start

For those who want to quickly have something running, the [Quick Start
page](quick_start) explains how to setup Dynare on Windows or macOS and to run your
first model file.


# <i class="fas fa-book"></i> Manual

The Dynare Manual is the best place to go to understand the Dynare syntax,
commands, and options. The `.pdf` and `.html` versions of the manual are
distributed with each Dynare release and also available here:

<p class="indent" markdown="1">
<i class="fas fa-file"></i> [HTML Manual](https://www.dynare.org/manual/)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [PDF Manual](https://www.dynare.org/manual.pdf)
</p>

# <i class="fas fa-book"></i> Dynare Team Presentations

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Dynare Youtube Channel with Summer School Presentations](https://www.youtube.com/c/DynareTeam)
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Deterministic Models: Perfect foresight,
nonlinearities and occasionally binding
constraints](https://git.dynare.org/sebastien/perfect-foresight-slides/raw/master/deterministic.pdf?inline=false),
by Sébastien Villemot
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Introduction to DSGE modeling (in
French)](https://git.dynare.org/sebastien/dsge-intro/raw/master/dsge-intro.pdf?inline=false),
by Sébastien Villemot
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Introduction to the internals of the Dynare
Preprocessor](../assets/team-presentations/preprocessor.pdf), by Houtan Bastani
and Sébastien Villemot
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Macro Processor
Tutorial](../assets/team-presentations/macroprocessor.pdf), by Houtan Bastani
and Sébastien Villemot
</p>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Tutorial on using the Preprocessor’s
JSON
output](https://macro.cepremap.fr/article/2020-01/dynare-preprocessor-w-json/),
by Houtan Bastani
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Guide to Specifying
Observation Equations for the Estimation of DSGE
Models](https://drive.google.com/file/d/1r89OU5OE3CBa6tOlj6l3hNVWEaRH5Anv/view?usp=sharing),
by Johannes Pfeifer
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Introduction to Graphs in
Dynare](https://drive.google.com/file/d/1DO1OK5AKLWfcJX49ZPCGo58Ni2LdERw4/view?usp=sharing),
by Johannes Pfeifer
</p>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Videos, tutorials and teaching materials for Dynare](https://mutschler.eu/dynare), by
Willi Mutscher
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Solving rational expectations models at first order: what Dynare does](https://www.dynare.org/wp-repo/dynarewp002.pdf),
by Sébastien Villemot
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i>
[Solving Stochastic Dynamic Equilibrium Models: A K-order Perturbation Approach](../assets/team-presentations/k_order.pdf),
a paper by Michel Juillard and Ondra Kamenik that describes the algorithm for k-order perturbation
</p>

<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i>
a description of the algorithm used for [solving the specialized Sylvester equation](../assets/team-presentations/sylvester.pdf), by Ondra Kamenik
</p>

# <i class="fas fa-book"></i> Dynare implementations of published models

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Ambrogio Cesa-Bianchi’s collection](https://sites.google.com/site/ambropo/replications)
</p>
<p class="indent">
Replication of
</p>
<ul class="resources">
 <li> Bernanke, Gertler, and Gilchrist, 1999. “The Financial Accelerator in a Quantitative Business Cycle Framework,” NBER Working Papers 6455</li>
 <li>Carlstrom and Fuerst, 1997. “Agency Costs, Net Worth, and Business Fluctuations: A Computable General Equilibrium Analysis,” American Economic Review,  vol. 87(5), pages 893-910, December</li>
 <li>Schmitt-Grohe, Stephanie & Uribe, Martin, 2003. “Closing small open economy models,” Journal of International Economics, vol. 61(1), pages 163-185, October</li>
 </ul>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Macroeconomic Model Data Base](https://www.macromodelbase.com/)
</p>
<p class="indent" markdown="1">
The database now covers more than 100 models, ranging from small-, medium- and
large-scale DSGE models to earlier-generation New-Keynesian models with
rational expectations and more traditional Keynesian-style models with adaptive
expectations. It includes models of the United States, the Euro Area, Canada,
and several small open emerging economies. Some of the models explicitly
incorporate financial frictions.
</p>

<p class="indent" markdown="1">
<i class="fas fa-external-link-alt"></i> [Johannes Pfeifer’s collection of Dynare models](https://github.com/johannespfeifer/dsge_mod)
</p>
<p class="indent" markdown="1">
A collection of Dynare models that demonstrates Dynare best practices,
providing mod files to replicate important models.
</p>


# <i class="fas fa-book"></i> Tutorial
Learn how to perform a stochastic simulation of a small model with Dynare in ten minutes!
<p class="indent" markdown="1">
<i class="fas fa-file-pdf"></i> [Tutorial](../assets/tutorial/guide.pdf)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [example1.mod](../assets/tutorial/example1.mod)
</p>
<p class="indent" markdown="1">
<i class="fas fa-file-download"></i> [example2.mod](../assets/tutorial/example2.mod)
</p>


# <i class="fab fa-wikipedia-w"></i> Wiki

The [Wiki](https://git.dynare.org/Dynare/dynare/wikis/home) contains various bits of information such as [new features](https://git.dynare.org/Dynare/dynare/wikis/NewFeatures), [development plans](https://git.dynare.org/Dynare/dynare/wikis/RoadMap), and [known](https://git.dynare.org/Dynare/dynare/wikis/KnownBugs) and [fixed](https://git.dynare.org/Dynare/dynare/wikis/FixedBugs) bugs.
