#!/usr/bin/env bash

set -e

cd _includes

for branch in release snapshot; do
    mkdir -p "$branch"
    cd "$branch"

    for dir in macos-x86_64 macos-arm64 source windows windows-7z windows-zip; do
        wget --no-verbose "https://www.dynare.org/$branch/$dir.json"
        readarray -t filenames < <(jq .[].filename "$dir.json")
        readarray -t timestamps < <(jq .[].date "$dir.json")
        readarray -t size < <(jq .[].size "$dir.json")
        n=$(jq length "$dir.json")
        includeFilename="download-$branch-$dir.html"
        echo > "$includeFilename"
        for ((i = 0; i < n; i++)); do
            filename=${filenames[i]%\"}
            filename=${filename#\"}
            mb=$((size[i] / (10 ** 6)))
            if [[ $branch == release ]]; then
                readarray -t -d - split <<< "$filename"
                if [[ $dir == windows* ]]; then
                    version=${split[1]}
                    ext=${filename##*.}
                elif [[ $dir == macos* ]]; then
                    version=${split[1]}
                    ext=${split[2]%%.*} # For macOS, we use the architecture as the extension (arm64 vs x86_64)
                    ext=${ext/_/-} # Transform x86_64 into x86-64
                else # Source
                    [[ ${split[1]} =~ ([0-9\.]+)\.([^[:space:]]+) ]]
                    version=${BASH_REMATCH[1]}
                    ext=${BASH_REMATCH[2]}
                    echo "{% assign dynare_stable_version = \"$version\" %}" > ../dynare_stable_version.md
                fi
                echo "<a href=\"https://www.dynare.org/$branch/$dir/$filename\">Dynare $version ($ext)</a> ($mb MB)" >> "$includeFilename"
            else
                timestamp=${timestamps[i]%\"}
                timestamp=${timestamp#\"}
                if [[ $OSTYPE == darwin* ]]; then
                    datestr=$(date -r "$timestamp")
                else
                    datestr=$(date -d @"$timestamp")
                fi
                {
                    echo "<div class=\"download_row\" onclick=\"document.location = 'https://www.dynare.org/$branch/$dir/$filename'\" onkeypress=\"document.location = 'https://www.dynare.org/$branch/$dir/$filename'\">"
                    echo "  <div class=\"download_cell_left\"><i class=\"fas fa-file-download\"></i>&nbsp;$datestr</div>"
                    echo "  <div class=\"download_cell_right\"><a href=\"#\">$filename</a> ($mb MB)</div>"
                    echo "</div>"
                } >> "$includeFilename"
            fi
        done
    done
    rm -- *json*
    cd ..
done
