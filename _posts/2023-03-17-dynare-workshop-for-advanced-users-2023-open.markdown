---
layout: posts
title:  "2nd Dynare Workshop for Advanced Users"
date:   2023-03-17
categories:
  - events
tags:
  - Dynare workshop
---

The workshop will take place in person from Wednesday to Friday September
6-8 2023. The event will be hosted by the Joint Research Centre of the
European Commission, Ispra (IT).

## Goals and format of the workshop

The workshop aims at presenting, discussing, and sharing experiences regarding
advanced features and expert use of Dynare. It welcomes and stimulates exchanges
and contributions with/from the audience.

The 2023 iteration of the workshop will focus on the following topics:

- An update on Dynare – New features and future plans

- Advanced estimation options:
  + Dive into optimizers, posterior sampling, and diagnostics options available in Dynare
  + Estimation of large models: trends, sectors, multi-country
  + GMM/SMM

- Simulating and estimating non-linear models in Dynare:
  + Use of stochastic simulations to explore stochastic properties of policies
  + Estimating models at higher order with nonlinear filters in Dynare. Feasibility/size constraints and best practices.

- Contributions from the audience and open discussion: “Estimating and simulating models for policy analysis and research: lessons learned, issues, needs.”
  + Roundtable
  + Selected in-depth presentations

## Audience

The workshop is addressed to advanced Dynare users with backgrounds ranging from
academia to policy institutions.

## Applications

The course will be open to a maximum of **20** qualified and selected participants.
Interested parties can apply by sending the following information to
[school@dynare.org](mailto:school@dynare.org):

- CV and a recent research paper (not necessarily using Dynare) (PDFs only)

- The [filled-in attached application form](https://dynare.adjemian.eu/form-dynare-workshop-2023.docx) (saved as a PDF)

Applications should be submitted no later than May 20th, 2023. We will notify
successful applicants by May 31st, 2023 the latest.

## Fee

No fee is due. Lunches, coffee breaks, a social dinner on September 7th, and
workshop material are provided.

## Venue

European Commission, Joint Research Centre,
Ispra site
via E. Fermi 2749
21027 Ispra (VA), Italy.

## Travel and transportation

Participants will have to fund their own travel and accommodation expenses. The
organizers have pre-booked rooms in nearby recommended hotels from September 5
until September 8, 2023. The workshop will start around 9:00 on Wednesday and
finish around 17:00 on Friday.

**Organizers will provide connections** between the
JRC and Milan Airports (Malpensa and Linate) and the Milano Centrale Train
Station on the days of arrival and departure. **We also organize two daily
connections** (morning and late afternoon) between the JRC and the pre-booked
hotels. There will be no connection provided from other hotels to/from JRC.

## Details

The course is jointly organized by the Joint Research Centre and CEPREMAP.
Organizers and animators:

- Stéphane Adjemian (Université du Mans)
- Michel Juillard (Banque de France)
- Willi Mutschler (Univ. Tübingen)
- Johannes Pfeifer (UniBW München)
- Marco Ratto (JRC)
- Sébastien Villemot (CEPREMAP).

Local organisers: Eleonora Beghetto, Katia Colombo, Adrian Ifrim, Beatrice
Pataracchia, Josselin Roman, Jan Teresinski.

This is a laptop-only workshop. Each participant is required to come with
his/her laptop with MATLAB R2014a or later and the latest stable Dynare version
installed. We will provide WiFi access, but participants shouldn’t rely on it to
access a MATLAB license server at their own institution. As an alternative to
MATLAB, it is possible to use GNU Octave (free software, compatible with MATLAB
syntax but slower).

Please note that electricity plugs are Schuko type F: attendees should make sure
to have the adapter to plug in in their laptop.
