---
layout: posts
title:  "Dynare Summer School 2023"
date:   2023-01-22
categories:
  - events
tags:
  - summer school
---

The Dynare Summer School 2023 will take place in person from May 22 to May
26, 2023. The event will be hosted by École Normale Supérieure (Paris).

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic
General Equilibrium (DSGE) modeling.

The course will focus on simulation and estimation of DSGE models with Dynare.
The school will also be an opportunity to introduce new features in Dynare.

This Summer school is aimed at beginners as well as at more experienced
researchers. PhD students are encouraged to participate. Time will be devoted to
the (Dynare related) problems encountered by students in their research.

## Application

Interested people should apply by sending an email to school@dynare.org.

Application should be done no later than March 3rd, 2023. You will have to
attach a CV and a recent research paper (please only send PDFs). We will confirm
acceptance the following week (no later than March 12th, 2023).

People working in organizations member of DSGE-net should register via their
network representative.

## Registration Fee

Registration fee (including lunches, coffee breaks and one diner): € 400 for
academics and € 1800 for institutional members.

## Workshop Animators

- [Stéphane Adjemian](https://stephane-adjemian.fr) (Université du Mans)
- [Michel Juillard](https://ideas.repec.org/e/pju1.html#research) (Banque de France)
- [Willi Mutschler](https://mutschler.eu/) (University of Tuebingen)
- [Marco Ratto](https://ideas.repec.org/f/pra217.html#research) (European Commission Joint Research Centre)
- [Normann Rion](https://normannrion.com/) (CEPREMAP)
- [Sébastien Villemot](https://sebastien.villemot.name/) (CEPREMAP)

This workshop is organized with the support of Banque de France, CEPREMAP and DSGE-net.

## Invited Speaker

- [Marco Del Negro](https://www.newyorkfed.org/research/economists/delnegro)
  (Federal Reserve Bank of New York)

## Preliminary program

Will be available soon.

## Venue

École Normale Supérieure<br />
48 boulevard Jourdan<br />
75014 Paris<br />
France

## Workshop Organization

This is a laptop only workshop. Each participant is required to come with
their laptop with MATLAB R2014a or later installed. We will provide WiFi
access, but participants shouldn’t rely on it to access a MATLAB license server
at their own institution. As an alternative to MATLAB, it is possible to use
GNU Octave (free software, compatible with MATLAB syntax).
