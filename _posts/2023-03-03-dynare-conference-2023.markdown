---
layout: posts
title:  "Dynare Conference 2023"
date:   2023-03-03
categories:
  - events
tags:
  - Dynare conference
---

The 17th annual Dynare Conference will be held in person at the [Central Bank of
Malta](https://www.centralbankmalta.org/), Valletta from 19th to 20th
October 2023. The conference is organized by the Economic Research Department of
the Central Bank of Malta, together with DSGE-net, and the Dynare project at
CEPREMAP.

Keynote speakers will be [John Hassler](http://hassler-j.iies.su.se/) (Institute
for International Economic Studies, Stockholm University) and [Frank
Smets](https://sites.google.com/view/frank-smets/) (European Central Bank, Ghent
University).

The program of the conference is available [here](https://dynare.adjemian.eu/dynare-conference-2023.pdf).

<strike>All papers should be submitted electronically using our [submission form](https://docs.google.com/forms/d/e/1FAIpQLScJsLvCPaJ-yJML_2IwKWK24b2qVskJjt_KIQvY_CbJh_9WXg/viewform) no later than midnight CET on 26th May 2023. Authors will be notified regarding the
outcome of their submissions by 16th June 2023. The Dynare conference will
feature the work of leading scholars in dynamic macroeconomic modelling and
provide an excellent opportunity to present your research results.</strike>

Topics of this Conference edition will focus on theoretical and empirical
analysis associated with:

 - Climate Change Policy and Modelling, Energy Policy, Commodities

 - Central Bank Digital Currency and Monetary policy

 - (Very) Small Open Economies, Core-Periphery in a monetary union

Submissions of papers dealing with different aspects of General Equilibrium
modelling and computational methods are also welcomed. Papers using software
tools other than Dynare and theoretical contributions are also encouraged.

For further details on the conference, please check out the Dynare website.

Accepted papers will be automatically considered for publication in the [Dynare
Working Paper series](https://www.dynare.org/wp/) conditional on the agreement of the submitter. Note that
publication in the Dynare Working Paper series does not prohibit submission to
another working paper series.

For any other requests, please contact: [conference@dynare.org](mailto:conference@dynare.org).
