---
layout: posts
title:  "Dynare Conference 2019"
date:   2019-02-05
categories:
  - events
tags:
  - dynare conference
excerpt: |
  <p>
  The 15th annual Dynare Conference will be held at the University of Lausanne, Switzerland, from September 9 to 10, 2019.
  </p>
---

The 15th annual Dynare Conference will be held at the University of Lausanne,
Switzerland, from September 9 to 10, 2019. The conference is organized
by HEC Lausanne, together with Banque de France, DSGE-net and the Dynare
project at CEPREMAP.

Program and details are available at
[http://dynare.mjui.fr](http://dynare.mjui.fr).

The Dynare conference will feature the work of leading scholars in
dynamic macroeconomic modeling and provide an excellent opportunity to
present your own research results.

Morten Ravn (University College London) and Gianluca Violante (Princeton University) will be plenary speakers.

This year’s conference focuses on the “heterogeneity” of e.g. households,
firms, information, expectations, banks and financial intermediaries,
etc.  Submissions of papers dealing with different aspects of DSGE
modeling and computational methods are welcome. Papers using
software tools other than Dynare or theoretical contributions are also
encouraged.

Paper submission procedure: please submit a complete manuscript or a
detailed abstract in PDF format at
[http://dynare.mjui.fr](http://dynare.mjui.fr). You must first
create an account on that server if you don't have one from last year.

Deadline for submissions is May 15, 2019. Authors of accepted papers
will be informed by June 15, 2019.

Accepted papers will be automatically considered for publication in
the [Dynare Working Paper series](https://www.dynare.org/wp)
conditional on the agreement of the submitter. Note that publication
in the Dynare Working Paper series does not prohibit submission to
another working paper series.

Contact: [conference@dynare.org](mailto:conference@dynare.org).

Conference organizers: Stéphane Adjemian (U. du Mans), Philippe
Bacchetta (U. Lausanne), Kenza Benhima (U. Lausanne), Florin Bilbiie
(U. Lausanne), Michel Juillard (Banque de France), Simon Scheidegger
(U. Lausanne).
