---
layout: posts
title:  "Virtual Course: Identification analysis and global sensitivity analysis for Macroeconomic Models"
date:   2020-09-15
categories:
  - events
tags:
  - GSA Workshop
---

From Nov. 25 to Nov. 27 2020.

In this virtual course, participants receive a general introduction to:

- methods of identification and global sensitivity analysis,
- their Dynare implementation (identification toolbox and global sensitivity analysis toolbox),
- their application to Dynamic Stochastic General Equilibrium (DSGE) macroeconomic models,
- techniques for improving the speed of estimations in Dynare.

More information available at:
[https://ec.europa.eu/jrc/en/event/webinar/dynare-gsa-macroeconomic-models-2020](https://ec.europa.eu/jrc/en/event/webinar/dynare-gsa-macroeconomic-models-2020)

The course is organised by the Joint Research Centre (JRC) of the European Commission. Organizers: Marco Ratto, Roberta Cardani, Olga Croitorov, Fabio Di Dio, Lorenzo Frattarolo, Massimo
Giovannini and Stefan Hohberger. Some members of the Dynare Team will also
participate as external speakers.

For application, please follow the link:

[https://ec.europa.eu/eusurvey/runner/JRCMACROGSACOURSE2020](https://ec.europa.eu/eusurvey/runner/JRCMACROGSACOURSE2020)
