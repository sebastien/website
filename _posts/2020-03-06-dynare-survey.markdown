---
layout: posts
title:  "Dynare Survey"
date:   2020-03-06
related: true
categories:
  - survey
tags:
  - survey
---

The Dynare Team would like to gain some insight into its user base and the
software environments its users are operating.

This will allow us to make better informed decisions about future development
plans. We highly appreciate your feedback. The survey is available at

[https://www.survey3.uni-koeln.de/index.php/569733?lang=en](https://www.survey3.uni-koeln.de/index.php/569733?lang=en)

and should only take a few minutes to complete. It is anonymous.

Please take some time to fill out the questionnaire.
