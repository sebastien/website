---
layout: posts
title:  "Job openings at the Joint Research Centre (European Commission)"
date:   2020-11-23
categories:
  - jobs
---

The Joint Research Centre (European Commission) seeks researchers with
expertise in economics/econometrics and data science to join its Finance and
Economy Unit. It is looking for new and experienced Ph.D. economists (or with
equivalent experience) with research and policy interests in the following
areas:

- Development of DSGE models to be used for macro-economic policy analysis;
- Development of nowcasting/forecasting models and techniques;
- Research in the fields of financial research, including sustainable finance
  and climate risk, financial stability, prudential policy and systemic risk
  more broadly;
- Research on corporate finance, resilience, the social dimension of the
  economic and monetary union.

Economists are expected to produce high-level academic and policy-oriented
research. Strong analytical and communication skills are essential. A Ph.D. or
a minimum of 5 years of research experience after the first degree giving
access to doctoral studies is required. Senior applicants should have a proven
publication track record.

The Finance and Economy Unit of the Joint Research Centre is located in Ispra,
Italy, and it offers an outstanding research environment and competitive
salaries and benefits. This position is for a period of up to 6 years. The Unit
provides support for the development of European Union policies based on
state-of-the-art financial and economic research. Its areas of expertise
include applied macroeconomics and time series econometrics, panel
econometrics, statistics, data analysis as well as financial modelling. The
mission of the Joint Research Centre is to support EU policies with independent
evidence throughout the whole policy cycle. The JRC is located in 5 Member
States (Belgium, Germany, Italy, the Netherlands and Spain). Further
information is available on the [JRC website](https://ec.europa.eu/jrc/).

Please provide a curriculum vitae, recent research papers, and two letters of
reference. Applications will be accepted online
[via this page on Econjobmarket](https://econjobmarket.org/positions/7039).
Follow the website instructions. The European Commission is an equal
opportunity employer.
