---
layout: posts
title:  "Dynare Summer School 2021"
date:   2021-03-25
categories:
  - events
tags:
  - summer school
---

The Dynare Summer School 2021 will take place from June 21-25, 2021. Due to the coronavirus outbreak, the summer school will be held online.

## Goals of the Summer School

The school will provide an introduction to Dynare and to Dynamic Stochastic
General Equilibrium (DSGE) modeling.

The course will focus on the simulation and estimation of DSGE models with
Dynare. The school will also be the occasion to introduce the next official
major release of Dynare (version 4.7).

The Dynare Summer School is aimed at beginners as well as at more experienced
researchers. PhD students are encouraged to participate.

## Preliminary program

TBD

## Application

Interested parties can apply by sending a CV and a recent research paper (PDFs
only) to [school@dynare.org](mailto:school@dynare.org).

Applications should be submitted no later than May 15, 2021. We will confirm
acceptance by May 30, 2021.

People working in organizations that are members of DSGE-net (Bank of Finland,
Banque de France, Capital Group, European Central Bank, Norges Bank, Sverige
Riksbank, Swiss National Bank) should register via their network
representative.

## Registration Fee

Since the summer school will take place online, there will be no registration fee this year.

## Animators

- [Stéphane Adjemian](https://stephane-adjemian.fr/) (Université du Maine)
- [Michel Juillard](https://ideas.repec.org/e/pju1.html#research) (Banque de France)
- [Frédéric Karamé](http://f.karame.free.fr/index.php) (Université du Maine)
- [Willi Mutschler](https://mutschler.eu/) (University of Tübingen)
- [Johannes Pfeifer](https://sites.google.com/site/pfeiferecon/) (Universität der Bundeswehr München)
- [Marco Ratto](https://ideas.repec.org/f/pra217.html#research) (European Commission Joint Research Centre)
- [Sébastien Villemot](https://sebastien.villemot.name/) (CEPREMAP)

This workshop is organized with the support of CEPREMAP, Banque de France, and
DSGE-net.


## Workshop Organization

Each participant is required to have an internet connection and a
machine equiped with MATLAB R2014a or later. As an alternative to
MATLAB, it is possible to use GNU Octave (free software, compatible
with MATLAB syntax).
