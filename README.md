# The Dynare Website

## Pre-requisites

- [Ruby](https://www.ruby-lang.org/en/)
- [Jekyll](https://jekyllrb.com/)
- Latex distribution (for compiling logo image with tikz) and `pdf2svg` (Debian: `apt-get install pdf2svg imagemagick`)
- If one encounters an error `attempt to perform an operation not allowed by the security policy 'PDF'`, then change the following line in `/etc/ImageMagick-6/policy.xml` from `<policy domain="coder" rights="none" pattern="PDF" />` to `<policy domain="coder" rights="read|write" pattern="PDF" />`, See [here](https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion) for more details.

## Setup Instructions

### Clone repository

```sh
git clone --recursive https://git.dynare.org/Dynare/website.git
cd website
```

### Jekyll and necessary Gems

```sh
gem install minimal-mistakes-jekyll
```

### Setup the files needed for the Working Papers page

```sh
(cd _data && wget --no-verbose --no-parent --accept '*.rdf' --recursive --no-directories --execute robots=off https://www.dynare.org/RePEc/cpm/dynare/ && ../assets/RePEc/rdf2yml.sh && rm -f *.rdf)
```

### Setup the files needed for the Download page

``` 
./setup-download-links.sh
```

### Create logo images

```sh
make -C assets/images/logo
```

### Mount website

```sh
jekyll serve
```

## To push version to server

```sh
JEKYLL_ENV=production jekyll build
```
Push the contents of `_site` folder to server
