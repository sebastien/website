---
title: Download
---
<br />

{% include dynare_stable_version.md %}

# Stable
The current stable release is Dynare {{ dynare_stable_version }}.

{% include download-stable.html %}

To download older versions of Dynare, go to the [release archives](https://www.dynare.org/release/).

<br />

<div class="dynare_download_page_row">
<div class="dynare_download_page_column" markdown="1">
# Referencing

To cite Dynare, use the following:

> Stéphane Adjemian, Houtan Bastani, Michel Juillard, Frédéric Karamé, Ferhat Mihoubi, Willi Mutschler, Johannes Pfeifer, Marco Ratto, Normann Rion and Sébastien Villemot (2022), “Dynare: Reference Manual, Version 5,” Dynare Working Papers, 72, CEPREMAP
<p class="stable" markdown="1"><i class="fas fa-file-download"></i> [BibTeX File](/assets/citation/dynare.bib)</p>
</div>
<div class="dynare_download_page_column" markdown="1">
# License

Dynare is free software, which means that it can be downloaded free of charge, that its source code is freely available, and that it can be used for both non-profit and for-profit purposes. Most of the source files are covered by the [GNU General Public Licence](https://www.gnu.org/licenses/gpl.html) version 3 or later (there are some exceptions to this, see the file [license.txt](https://git.dynare.org/Dynare/dynare/blob/master/license.txt) in Dynare distribution).
</div>
</div>

<br />
# Unstable

The unstable version of Dynare is the version on which Dynare developers are
currently working. It contains the latest features, but may contain bugs or
even not work at all! For this reason, most users should use the stable version
unless they really need bleeding edge features and are willing to take some
risks. It is produced whenever there is a change to the codebase.

{% include download-snapshot.html %}
