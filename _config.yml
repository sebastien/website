# Welcome to Jekyll!
#
# This config file is meant for settings that affect your entire site, values
# which you are expected to set up once and rarely need to edit after that.
# For technical reasons, this file is *NOT* reloaded automatically when you use
# `jekyll serve`. If you change this file, please restart the server process.

# Theme Settings
#
# Review documentation to determine if you should use `theme` or `remote_theme`
# https://mmistakes.github.io/minimal-mistakes/docs/quick-start-guide/#installing-the-theme

# theme                  : "minimal-mistakes-jekyll"
# remote_theme           : "mmistakes/minimal-mistakes"
minimal_mistakes_skin    : "contrast" # "default" # "air", "aqua", "contrast", "dark", "dirt", "neon", "mint", "plum", "sunrise"

# Site Settings
locale                   : "en-US"
title                    : "Dynare"
title_separator          : "|"
name                     : "Dynare"
logo                     : "/assets/images/logo/dlogo.svg"
description              : "Dynare is a software platform for handling a wide class of economic models, in particular dynamic stochastic general equilibrium (DSGE) and overlapping generations (OLG) models"
url                      : "https://www.dynare.org" # the base hostname & protocol for your site e.g. "https://mmistakes.github.io"
baseurl                  : # the subpath of your site, e.g. "/blog"
repository               : "https://git.dynare.org/Dynare/dynare" # GitHub username/repo-name e.g. "mmistakes/minimal-mistakes"
copyright_begin          : 1996
teaser                   : # path of fallback teaser image, e.g. "/assets/images/500x300.png"
breadcrumbs              : false
words_per_minute         : 200
comments:
  provider               : # false (default), "disqus", "discourse", "facebook", "google-plus", "staticman", "staticman_v2" "custom"
  disqus:
    shortname            : # https://help.disqus.com/customer/portal/articles/466208-what-s-a-shortname-
  discourse:
    server               : # https://meta.discourse.org/t/embedding-discourse-comments-via-javascript/31963 , e.g.: meta.discourse.org
  facebook:
    # https://developers.facebook.com/docs/plugins/comments
    appid                :
    num_posts            : # 5 (default)
    colorscheme          : # "light" (default), "dark"
staticman:
  allowedFields          : # ['name', 'email', 'url', 'message']
  branch                 : # "master"
  commitMessage          : # "New comment by {fields.name}"
  filename               : # comment-{@timestamp}
  format                 : # "yml"
  moderation             : # true
  path                   : # "/_data/comments/{options.slug}" (default)
  requiredFields         : # ['name', 'email', 'message']
  transforms:
    email                : # "md5"
  generatedFields:
    date:
      type               : # "date"
      options:
        format           : # "iso8601" (default), "timestamp-seconds", "timestamp-milliseconds"
  endpoint               : # URL of your own deployment with trailing slash, will fallback to the public instance
reCaptcha:
  siteKey                :
  secret                 :
atom_feed:
  path                   : # blank (default) uses feed.xml
search                   : false # true, false (default)
search_full_content      : true # true, false (default)
search_provider          : lunr # lunr (default), algolia, google
algolia:
  application_id         : # YOUR_APPLICATION_ID
  index_name             : # YOUR_INDEX_NAME
  search_only_api_key    : # YOUR_SEARCH_ONLY_API_KEY
  powered_by             : # true (default), false
google:
  search_engine_id       : # YOUR_SEARCH_ENGINE_ID
  instant_search         : # false (default), true
# SEO Related
google_site_verification :
bing_site_verification   :
yandex_site_verification :
naver_site_verification  :

# Social Sharing
twitter:
  username               :
facebook:
  username               :
  app_id                 :
  publisher              :
og_image                 : "/assets/images/logo/dlogo.svg" # Open Graph/Twitter default site image

# For specifying social profiles
# - https://developers.google.com/structured-data/customize/social-profiles
social:
  type                   : Organization # Person or Organization (defaults to Person)
  name                   : Dynare # If the user or organization name differs from the site's name
  links: # An array of links to social media profiles

# Analytics
analytics:
  provider               : false # false (default), "google", "google-universal", "custom"
  google:
    tracking_id          :
    anonymize_ip         : # true, false (default)


# Site Author
author:
  name             :
  avatar           :
  bio              :
  location         :
  email            :

# Site Footer
footer:
  links:
    - label: "Forum"
      icon: "fab fa-discourse"
      url: https://forum.dynare.org/
    - label: "Git"
      icon: "fab fa-gitlab"
      url: https://git.dynare.org/Dynare/dynare
    - label: "Wiki"
      icon: "fab fa-wikipedia-w"
      url: https://git.dynare.org/Dynare/dynare/wikis/home
    - label: "Known Bugs"
      icon: "fas fa-bug"
      url: https://git.dynare.org/Dynare/dynare/-/wikis/Known-bugs-present-in-the-current-stable-version
    - label: "Twitter/X"
      icon: "fab fa-x-twitter"
      url: https://twitter.com/DynareTeam
    - label: "Mastodon"
      icon: "fab fa-mastodon"
      url: https://sciences.social/@dynare

# Reading Files
include:
  - .htaccess
  - _pages
exclude:
  - "*.sublime-project"
  - "*.sublime-workspace"
  - vendor
  - .asset-cache
  - .bundle
  - .jekyll-assets-cache
  - .sass-cache
  - assets/js/plugins
  - assets/js/_main.js
  - assets/js/vendor
  - Capfile
  - CHANGELOG
  - config
  - Gemfile
  - Gruntfile.js
  - gulpfile.js
  - LICENSE
  - log
  - node_modules
  - package.json
  - Rakefile
  - README
  - tmp
  - /docs # ignore Minimal Mistakes /docs
  - /test # ignore Minimal Mistakes /test
  - /assets/images/logo/*.aux
  - /assets/images/logo/*.log
  - /assets/images/logo/*.pdf
  - /assets/images/logo/*.tex
  - /assets/images/logo/*.sh
  - /assets/images/logo/Makefile
  - /assets/images/logo/dbkgd_ws.png
  - /assets/RePEc/rdf2yml.sh
  - .gitlab-ci.yml
  - setup-download-links.sh
  - COPYING
keep_files:
  - .git
  - /assets/images/*
  - /assets/images/logo/*
encoding: "utf-8"
markdown_ext: "markdown,mkdown,mkdn,mkd,md"


# Conversion
markdown: kramdown
highlighter: rouge
lsi: false
excerpt_separator: "\n\n"
incremental: false


# Markdown Processing
kramdown:
  input: GFM
  hard_wrap: false
  auto_ids: true
  footnote_nr: 1
  entity_output: as_char
  toc_levels: 1..6
  smart_quotes: lsquo,rsquo,ldquo,rdquo
  enable_coderay: false

# Sass/SCSS
sass:
  sass_dir: _sass
  style: compressed # http://sass-lang.com/documentation/file.SASS_REFERENCE.html#output_style


# Outputting
permalink: /:categories/:title/
#paginate: 10 # amount of posts to show
#paginate_path: /news/page:num
timezone: Europe/Paris # https://en.wikipedia.org/wiki/List_of_tz_database_time_zones

theme: minimal-mistakes-jekyll

plugins:
  - jekyll-sitemap
  - jekyll-archives
  - jekyll-include-cache

# mimic GitHub Pages with --safe
whitelist:
  - jekyll-sitemap
  - jekyll-archives
  - jekyll-include-cache

# Archives
#  Type
#  - GitHub Pages compatible archive pages built with Liquid ~> type: liquid (default)
#  - Jekyll Archives plugin archive pages ~> type: jekyll-archives
#  Path (examples)
#  - Archive page should exist at path when using Liquid method or you can
#    expect broken links (especially with breadcrumbs enabled)
#  - <base_path>/tags/my-awesome-tag/index.html ~> path: /tags/
#  - <base_path/categories/my-awesome-category/index.html ~> path: /categories/
#  - <base_path/my-awesome-category/index.html ~> path: /
category_archive:
  type: jekyll-archives
  path: /categories/
tag_archive:
  type: jekyll-archives
  path: /tags/
# https://github.com/jekyll/jekyll-archives
jekyll-archives:
  enabled:
    - categories
    - tags
  layouts:
    category: archive-taxonomy
    tag: archive-taxonomy
  permalinks:
    category: /categories/:name/
    tag: /tags/:name/


# HTML Compression
# - http://jch.penibelst.de/
compress_html:
  clippings: all
  ignore:
    envs: development


# Defaults
defaults:
  # _posts
  - scope:
      path: ""
      type: posts
    values:
      layout: splash
      author_profile: false
      read_time: false
      share: true
      related: true
  # _pages
  - scope:
      path: ""
      type: pages
    values:
      layout: splash
      author_profile: false
  # snapshots
  - scope:
      path: "assets/snapshot/windows"
    values:
      snapshot: "windows-exe"
  - scope:
      path: "assets/snapshot/windows-zip"
    values:
      snapshot: "windows-zip"
  - scope:
      path: "assets/snapshot/macos"
    values:
      snapshot: "macos"
  - scope:
      path: "assets/snapshot/source"
    values:
      snapshot: "source"
  # release
  - scope:
      path: "assets/release/windows"
    values:
      release: "windows-exe"
  - scope:
      path: "assets/release/windows-zip"
    values:
      release: "windows-zip"
  - scope:
      path: "assets/release/macos"
    values:
      release: "macos"
  - scope:
      path: "assets/release/source"
    values:
      release: "source"
